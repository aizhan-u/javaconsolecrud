package DBEstablishment;

import java.sql.Connection;
import java.sql.Statement;

public class DropTables {

    public static void dropTables(Connection connection) {
        dropUsersTable(connection);
        dropLoginsTable(connection);
        dropItemsTable(connection);
        dropUserCartTable(connection);
    }

    private static void dropUsersTable(Connection connection) {
        try {
            String query = "drop table if exists users cascade";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void dropLoginsTable(Connection connection) {
        try {
            String query = "drop table if exists logins cascade";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void dropItemsTable(Connection connection) {
        try {
            String query = "drop table if exists items cascade";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void dropUserCartTable(Connection connection) {
        try {
            String query = "drop table if exists user_cart cascade";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
