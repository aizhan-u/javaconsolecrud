package main;

import DBEstablishment.*;
import DBManipulation.*;

import java.sql.Connection;
import java.util.Scanner;

public class
Main {
    static final String[] tables = {"users", "logins", "items", "user_cart"};
    static final String[] commands = {"add", "edit", "delete", "exit the program"};

    public static void main(String[] args) {
        DBConnect dbConnect = new DBConnect();
        Connection connection = dbConnect.getConnection();
        CreateTables.createTables(connection);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please, select the table or type 0 to exit");
        printContent(tables);
        int tableName = scanner.nextInt();

        int command, id;
        while(tableName != 0) {
            String[] columns = SelectValues.showTableContent(connection, tables[tableName-1]);

            System.out.println("\nPlease, select what you want to do or press 0 to go back");
            printContent(commands);
            command = scanner.nextInt();
            while(command != 0) {
                switch (command) {
                    case 1: // add
                        String[] values = new String[columns.length-1];
                        scanner.nextLine();
                        for(int i = 1; i < columns.length; i ++) {
                            System.out.println("Please, enter a value for " + columns[i]);
                            System.out.println("Dates should be written in the format DD-MMM-YYYY");
                            values[i-1] = scanner.nextLine();
                        }
                        InsertValues.InsertTableValue(connection, tables[tableName-1], columns, values);
                        break;
                    case 2: // edit
                        SelectValues.showTableContent(connection, tables[tableName-1]);
                        System.out.println("\nPlease, select the id you want to edit or press 0 to go back");
                        id = scanner.nextInt();
                        if(id != 0) {
                            String newValue;
                            int column;
                            SelectValues.showRowContent(connection, tables[tableName - 1], id);
                            System.out.println("Please, select the column value of which you want to change or press 0 to go back");
                            column = scanner.nextInt();
                            if(column != 0) {
                                System.out.println("Enter the new value");
                                scanner.nextLine();
                                newValue = scanner.nextLine();
                                UpdateValues.updateRowValue(connection, tables[tableName - 1], id, columns[column-1], newValue);
                            }
                        }
                        break;
                    case 3: // delete
                        SelectValues.showTableContent(connection, tables[tableName-1]);
                        System.out.println("\nPlease, select the id you want to delete or press 0 to go back");
                        id = scanner.nextInt();
                        if(id != 0)
                            DeleteValues.deleteValue(connection, tables[tableName-1], id);
                        break;
                    case 4: // exit
                        return;
                }
                SelectValues.showTableContent(connection, tables[tableName-1]);
                System.out.println("\nPlease, select what you want to do or press 0 to go back");
                printContent(commands);
                command = scanner.nextInt();
            }

            System.out.println("\nPlease, select the table or type 0 to exit");
            printContent(tables);
            tableName = scanner.nextInt();
        }
    }

    private static void printContent(String[] out) {
        for(int i = 0; i < out.length; i++) {
            System.out.println((i + 1) + ": " + out[i]);
        }
    }
}
