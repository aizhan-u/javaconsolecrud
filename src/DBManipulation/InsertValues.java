package DBManipulation;

import java.sql.Connection;
import java.sql.Statement;

public class InsertValues {
    public static void InsertTableValue(Connection connection, String tablename, String[] cols, String[] vals) {
        try {
            String query = "INSERT into " + tablename + "(";
            for(int i = 1; i < cols.length; i ++) {
                query += cols[i];
                if(i != cols.length - 1) {
                    query += ", ";
                }
            }
            query += ") VALUES (";
            for(int i = 0; i < vals.length; i ++) {
                query = query + "'" + vals[i] + "'";
                if(i != vals.length - 1) {
                    query += ", ";
                }
            }
            query += ");";

            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
            System.out.println("Insert successful");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
