package DBManipulation;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class SelectValues {
    public static String[] showTableContent(Connection connection, String tablename) {
        String[] res = {};
        try {
            String query = "SELECT * from " + tablename + ";";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int count = rsmd.getColumnCount();
            res = new String[count];

            for(int i = 0; i < count; i ++) {
                res[i] = rsmd.getColumnName(i + 1);
                System.out.print(res[i] + "\t");
            }
            System.out.println("");

            while(rs.next()) {
                for(int i = 0; i < count; i ++) {
                    System.out.print(rs.getString(i + 1) + "\t");
                }
                System.out.println("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static void showRowContent(Connection connection, String tablename, int id) {
        try {
            String query = "SELECT * from " + tablename + " where id = " + id + ";";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int count = rsmd.getColumnCount();

            rs.next();
            System.out.println("Column\tValue");
            for(int i = 1; i <= count; i ++) {
                System.out.println((i) + ": " + rsmd.getColumnName(i) + "\t" + rs.getString(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
