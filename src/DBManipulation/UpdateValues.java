package DBManipulation;

import java.sql.Connection;
import java.sql.Statement;

public class UpdateValues {
    public static void updateRowValue(Connection connection, String tablename, int id, String col, String newValue) {
        try {
            String query = "UPDATE " + tablename + " set " + col + " = '" + newValue + "' where id = " + id + ";";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
            System.out.println("Update successful");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
